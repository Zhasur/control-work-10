CREATE SCHEMA `news` DEFAULT CHARACTER SET utf8 ;

USE `news`;

CREATE TABLE `news_table` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) NOT NULL,
  `description` TEXT NOT NULL,
  `image` VARCHAR(150) NULL,
  `date` DATETIME NULL,
  PRIMARY KEY (`id`));

  CREATE TABLE `comments` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `news_id` INT NOT NULL,
  `author` VARCHAR(255) NULL,
  `comments` TEXT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `news_id_fk_idx` (`news_id` ASC),
  CONSTRAINT `news_id_fk`
    FOREIGN KEY (`news_id`)
    REFERENCES `news_table` (`id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE);

    INSERT INTO `news_table` (`id`, `title`, `description`, `date`)
    VALUES
		(1,
        'Армия США возвращается к стратегии времен холодной войны',
        'Военные США переходят к стратегии Пентагона «Динамичное развертывание сил»,
        которая применялась американскими войсками в период холодной войны, пишет Stars and Stripes,
        официальная газета минобороны Соединенных Штатов...', now()),
        (2,
        'Саакашвили показал пользу безгалстуковой диеты',
        'Бывший президент Грузии, экс-губернатор Одесской области Украины Михаил Саакашвили в интервью
        украинскому изданию «Гордон» продемонстрировал пользу отказа от галстуков.

		Политик вспомнил «пятидневную войну» в августе 2008 года и кадры, на которых он разговаривал по телефону
        и жевал галстук. Он рассказал, что не знал о том, что он в прямом эфире,
        а утратив контроль над собой из-за некой важной услышанной информации, «человек может что угодно сделать».
        При этом он похвастался, заявив, что другие на его месте нагадили бы в штаны.', now());

        INSERT INTO `comments` (`id`, `news_id`, `author`, `comments`)
        VALUES
			(1, 2, 'lenta.ru', 'Что происходит в России и в мире?'),
            (2, 1, 'gazeta.ru', 'Ранее в Пентагоне заявили, что Россия не планирует атаковать страны НАТО.');
