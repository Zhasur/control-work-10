import React, {Component, Fragment} from 'react';
import AddNewsForm from "../../components/AddNews/AddNewsForm";
import {createNews} from "../../store/actions/newsActions";
import {connect} from "react-redux";

class CurrentAddedNews extends Component {

    createNews = newsData => {
        this.props.onNewsCreated(newsData).then(() => {
            this.props.history.push('/');
        })
    };

    render() {
        return (
            <Fragment>
                <h2>New event</h2>
                <AddNewsForm onSubmit={this.createNews}/>
            </Fragment>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    onNewsCreated: newsData => dispatch(createNews(newsData))
});

export default connect(null, mapDispatchToProps)(CurrentAddedNews);