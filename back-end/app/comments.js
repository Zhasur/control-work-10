const express = require('express');
const router = express.Router();

const createRouter = connection => {
    router.get('/', (req, res) => {
        connection.query('SELECT * FROM comments', (err, result) => {
            if (err) {
                res.status(500).send({error: "Database error"});
            }

            const comments = [];

            result.map(item => {
                if (item.author) {
                    comments.push({id: item.id, news_id: item.news_id, author: item.author, comments: item.comments})
                } else if (item.author === null) {
                    const author = 'Anonymous';
                    comments.push({id: item.id, news_id: item.news_id, author: author, comments: item.comments})
                }
            });

            res.send(comments)
        })
    });

    router.post('/', (req,res) => {
        const comments = req.body;
        connection.query('INSERT INTO comments (news_id, comments) VALUES (?,?)',
            [comments.news_id, comments.comments],
            (err , result) => {
                if (err) {
                    res.status(500).send({error: 'Database error'})
                }
                res.send({message: 'OK'});
            });
    });

    router.get('/:id', (req, res) => {
        connection.query('SELECT * FROM comments WHERE `news_id` = ?', req.params.id, (err, result) => {
            if (err) {
                res.status(500).send({error: "Database error"});
            }
            res.send(result[0])
        })
    });

    router.delete('/:id', (req, res) => {
        connection.query('DELETE FROM `comments` WHERE `id` = ?', req.params.id, (err, result) => {
            if (err) {
                res.status(500).send({error: "Database error"});
            }
            res.send(result[0])
        })
    });

    return router
};


module.exports = createRouter;