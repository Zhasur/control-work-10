const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const config = require('../config');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath)
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const createRouter = connection => {
    const router = express.Router();

    router.get('/', (req, res) => {
        connection.query('SELECT * FROM news_table', (err, result) => {
            if (err) {
                res.status(500).send({error: "Database error"});
            }

            result.map(post => {
                if (post.date === null) {
                    const date = new Date();
                    date.toISOString();
                    post.date = date
                }

            });

            res.send(result)
        })
    });

    router.get('/:id', (req, res) => {
        connection.query('SELECT * FROM news_table WHERE `id` = ?', req.params.id, (err, result) => {
            if (err) {
                res.status(500).send({error: "Database error"});
            }
            res.send(result[0])
        })
    });

    router.post('/', upload.single('image'), (req,res) => {
        const news = req.body;
        if (req.file) {
            news.image = req.file.filename
        }

        connection.query('INSERT INTO news_table (`title`, `description`) VALUES (?,?)',
            [news.title, news.description],
            (err , result) => {

                if (err) {
                    res.status(500).send({error: 'Database error'})
                }
                res.send({message: 'OK'});
            });
    });

    router.delete('/:id', (req, res) => {
        connection.query('DELETE FROM `news_table` WHERE `id` = ?', req.params.id, (err, result) => {
            if (err) {
                res.status(500).send({error: "Database error"});
            }
            res.send(result[0])
        })
    });

    return router
};

module.exports = createRouter;