import React, {Component} from 'react';
import {fetchSingleNews} from "../../store/actions/newsActions";
import {connect} from "react-redux";
import {createComment, deleteComment, fetchComments} from "../../store/actions/commentAction";
import {Button, Col, Form, FormGroup, Input, Label} from "reactstrap";

class SinglePost extends Component {
    state = {
        name: this.props.comments.author,
        comment: this.props.comments.comments,
    };

    componentDidMount() {
        let id = this.props.match.params.id;
        this.props.fetchComments(id);
    }

    render() {
        let post;
        if (this.props.post) {
            post = this.props.post;
        }

        let comment;
        let Comments;
        if (this.props.comments) {
            comment = this.props.comments;
            if (comment.author === null) {
                comment.author = 'Anonymous'
            }
            Comments = (
                <div>
                    <li style={{fontWeight: "700"}}>{comment.author} wrote: <span style={{fontWeight: "400"}}>{comment.comments}</span></li>
                    <button onClick={() => this.props.deleteComment(comment.id)}>Delete comment</button>
                </div>
            )
        } else {
            Comments = 'No comments yet!'
        }

        return (
            <div key={post.id}>
                <h4 style={{display: "block"}}><strong>Title: </strong>{post.title}</h4>

                <p style={{display: "block", fontSize: "20px"}}><strong style={{fontSize: "20px"}}>Description:</strong> {post.description}</p>
                <p><strong>Date:</strong> {post.date}</p>

                <div className="comments">
                    <ul>
                        {Comments}
                    </ul>
                </div>
                <Form>
                    <FormGroup row>
                        <Label sm={2} for="title">Name</Label>
                        <Col sm={10}>
                            <Input
                                type="text" required
                                name="title" id="title"
                                placeholder="Enter news title"
                                onChange={this.inputChangeHandler}
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label sm={2} for="description">Comment</Label>
                        <Col sm={10}>
                            <Input
                                type="textarea" required
                                name="description" id="description"
                                placeholder="Enter description"
                                onChange={this.inputChangeHandler}
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Col sm={{offset: 2, size: 10}}>
                            <Button type="submit" color="primary" onClick={this.props.createComment}>Add</Button>
                        </Col>
                    </FormGroup>
                </Form>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    post: state.news.post,
    comments: state.comments.comments
});

const mapDispatchToProps = dispatch => ({
    fetchSingleNews: (id) => dispatch(fetchSingleNews(id)),
    fetchComments: (id) => dispatch(fetchComments(id)),
    deleteComment: (id) => dispatch(deleteComment(id)),
    createComment: () => dispatch(createComment()),
});

export default connect(mapStateToProps, mapDispatchToProps)(SinglePost);