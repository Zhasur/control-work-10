import React, {Component, Fragment} from 'react';
import {Container} from "reactstrap";
import {Route, Switch} from "react-router-dom";
import './App.css'

import Toolbar from "./components/UI/Toolbar/Toolbar";
import News from "./containers/News/News";
import CurrentAddedNews from "./containers/CurrentAddedNews/CurrentAddedNews";
import SinglePost from "./containers/Post/SinglePost";

class App extends Component {
    render() {
        return (
            <Fragment>
                <header>
                    <Toolbar/>
                </header>
                <Container style={{marginTop: '20px'}}>
                    <Switch>
                        <Route path="/" exact component={News} />
                        <Route path="/news/:id" exact component={SinglePost} />
                        <Route path="/add/news" exact component={CurrentAddedNews} />
                    </Switch>
                </Container>
            </Fragment>
        );
    }
}

export default App;
