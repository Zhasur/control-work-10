import React, {Component, Fragment} from 'react';
import {Button, Card, CardBody} from "reactstrap";
import {deleteSingleNews, fetchNews, fetchSingleNews} from "../../store/actions/newsActions";
import {connect} from "react-redux";
import {Link} from "react-router-dom";
import NewsThumbnail from "../../components/NewsThumbnail/NewsThumbnail";

class News extends Component {

    componentDidMount() {
        this.props.onFetchNews();
    }

    render() {
        let news;
        if (this.props.news) {
            news = this.props.news.map(post => (
                <Card key={post.id} style={{marginBottom: '10px'}}>
                    <CardBody>
                        <div className="image" style={{display: "inline-block"}}>
                            <NewsThumbnail image={post.image} />

                        </div>
                        <div className="inner" style={{display: "inline-block", verticalAlign: "middle"}}>
                            <h4>
                                {post.title}
                            </h4>
                            <span style={{display: "block", marginLeft: '10px'}}>
                                <span style={{fontSize: "18px"}}>At</span> {post.date}
                            </span>
                            <Link
                                to={'/news/' + post.id}
                            >
                                <Button color="success" onClick={() => this.props.fetchSingleNews(post.id)}>
                                    Read Full Post >>
                                </Button>
                            </Link>
                            <Button onClick={() => this.props.deleteSingleNews(post.id)} style={{marginLeft: "10px"}} color="danger">Delete</Button>
                        </div>
                    </CardBody>
                </Card>
            ));

        }

        return (
            <Fragment>
                <h2>
                    News
                    <Link
                        to="/add/news"
                    >
                        <Button
                            color="primary"
                            className="float-lg-right"
                        >
                            Add news
                        </Button>
                    </Link>
                </h2>
                {news}
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    news: state.news.news
});

const mapDispatchToProps = dispatch => ({
    onFetchNews: () => dispatch(fetchNews()),
    fetchSingleNews: (id) => dispatch(fetchSingleNews(id)),
    deleteSingleNews: (id) => dispatch(deleteSingleNews(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(News);
