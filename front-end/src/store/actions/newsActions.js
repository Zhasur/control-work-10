import axios from '../../news-axios';

export const FETCH_NEWS_SUCCESS = 'FETCH_NEWS_SUCCESS';
export const CREATE_NEWS_SUCCESS = 'CREATE_NEWS_SUCCESS';
export const FETCH_POST_SUCCESS = 'FETCH_POST_SUCCESS';
export const DELETE_POST_SUCCESS = 'DELETE_POST_SUCCESS';

export const fetchNewsSuccess = news => ({type: FETCH_NEWS_SUCCESS, news});
export const fetchPostSuccess = post_id => ({type: FETCH_POST_SUCCESS, post_id});
export const deleteSuccess = post_id => ({type: DELETE_POST_SUCCESS, post_id});
export const createNewsSuccess = () => ({type: CREATE_NEWS_SUCCESS});

export const fetchNews = () => {
    return dispatch => {
        return axios.get('/news').then(
            response => dispatch(fetchNewsSuccess(response.data))
        )
    }
};

export const createNews = newsData => {
    return dispatch => {
        return axios.post('/news', newsData).then(
            () => dispatch(createNewsSuccess())
        )
    }
};

export const fetchSingleNews = (id) => {
    return dispatch => {
        return axios.get(`/news/${id}`).then(response => {
            dispatch(fetchPostSuccess(response.data))
            }
        )
    }
};
export const deleteSingleNews = (id) => {
    return dispatch => {
        return axios.delete(`/news/${id}`).then(response => {
                dispatch(deleteSuccess(response.data));
                window.location.reload()
            }
        )
    }
};
