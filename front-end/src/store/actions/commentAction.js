import axios from '../../news-axios';

export const FETCH_COMMENTS_SUCCESS = 'FETCH_COMMENTS_SUCCESS';
export const POST_COMMENTS_SUCCESS = 'POST_COMMENTS_SUCCESS ';
export const DELETE_COMMENTS_SUCCESS = 'DELETE_COMMENTS_SUCCESS';

export const fetchCommentsSuccess = comments => ({type: FETCH_COMMENTS_SUCCESS, comments});
export const deleteSuccess = id => ({type: DELETE_COMMENTS_SUCCESS, id});
export const createSuccess = () => ({type: POST_COMMENTS_SUCCESS});


export const fetchComments = (id) => {
    return dispatch => {
        return axios.get('/comments/' + id).then(
            response => dispatch(fetchCommentsSuccess(response.data))
        )
    }
};

export const createComment = newsData => {
    return dispatch => {
        return axios.post('/comments', newsData).then(
            () => dispatch(createSuccess())
        )
    }
};

export const deleteComment = (id) => {
    return dispatch => {
        return axios.delete(`/comments/${id}`).then(response => {
                dispatch(deleteSuccess(response.data));
                window.location.reload()
            }
        )
    }
};
