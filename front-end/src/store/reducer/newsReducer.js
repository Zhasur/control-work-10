import {DELETE_POST_SUCCESS, FETCH_NEWS_SUCCESS, FETCH_POST_SUCCESS} from "../actions/newsActions";

const initialState = {
    news : [],
    post: {}
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_NEWS_SUCCESS:
            return {...state, news: action.news};
        case FETCH_POST_SUCCESS:
            return{...state, post: action.post_id};
        case DELETE_POST_SUCCESS:
            return{...state, news: action.post_id};
        default:
            return state;
    }
};

export default reducer;