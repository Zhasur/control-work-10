import {DELETE_COMMENTS_SUCCESS, FETCH_COMMENTS_SUCCESS} from "../actions/commentAction";

const initialState = {
    comments : [],
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_COMMENTS_SUCCESS:
            return {...state, comments: action.comments};
        case DELETE_COMMENTS_SUCCESS:
            return{...state, comments: action.id};
        default:
            return state;
    }
};

export default reducer;